import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qalki/models/user.dart';
import 'package:qalki/screens/home.dart';

class AuthWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    print(user);

    // return either the Home or Authenticate widget
    if (user == null) {
      return MyHomePage();
    } else {
      return MyHomePage();
    }
  }
}
