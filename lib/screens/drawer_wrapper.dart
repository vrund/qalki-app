import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qalki/screens/drawer_page_holder.dart';
import 'package:qalki/widgets/drawer.dart';

class SelectedDrawerItemModel {
  String _selectedDrawerItem = 'library';

  String selectedDrawerItem() => this._selectedDrawerItem;

  void updateItem(String newSelectedItem) {
    print('updating item');
    this._selectedDrawerItem = newSelectedItem;
  }
}

class DrawerWrapper extends StatelessWidget {
  const DrawerWrapper({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider<SelectedDrawerItemModel>(
      create: (BuildContext context) => SelectedDrawerItemModel(),
      child: Scaffold(
        drawer: Consumer<SelectedDrawerItemModel>(
          builder: (BuildContext context, SelectedDrawerItemModel value,
              Widget child) {
            return ProfileDrawer(selectedDrawerItemModel: value);
          },
        ),
        body: Consumer<SelectedDrawerItemModel>(
          builder: (BuildContext context, SelectedDrawerItemModel value,
              Widget child) {
            return DrawerPageHolder(
                selectedDrawerItem: value.selectedDrawerItem());
          },
        ),
      ),
    );
  }
}
