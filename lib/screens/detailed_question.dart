import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:qalki/locator.dart';
import 'package:qalki/models/discussion_post.dart';
import 'package:qalki/services/discussion_service.dart';
import 'package:qalki/utils.dart';

import '../routes.dart';

class DetailedQuestionPage extends StatefulWidget {
  DetailedQuestionPage({Key key}) : super(key: key);
  @override
  _DetailedQuestionPageState createState() => _DetailedQuestionPageState();
}

class _DetailedQuestionPageState extends State<DetailedQuestionPage> {
  final dbRef = Firestore.instance;
  final discussionService = locator<DiscussionService>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: hexToColor('#0D47A1'),
        elevation: 0.0,
        title: Text('Question'),
      ),
      body: FutureBuilder<DiscussionPost>(
        future: discussionService
            .getDocumentById('0Ap2qqjUZDfRgZ9qE4mj')
            .then((snapshot) => DiscussionPost.fromJson(snapshot)),
        builder: (context, post) {
          if (!post.hasData) {
            print(post);
            return Text('Loading...');
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    color: hexToColor('#0D47A1'),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              GestureDetector(
                                child: CircleAvatar(
                                  backgroundImage:
                                      AssetImage('assets/images/person_1.jpg'),
                                  radius: 25,
                                ),
                                onTap: () {
                                  Routes.sailor('/user-topics-discussion');
                                },
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(post.data.author),
                                    Text('@vrundpatel')
                                  ],
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(
                                10.0, 20.0, 10.0, 10.0),
                            child: Text(
                              post.data.question,
                              textDirection: TextDirection.ltr,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Text('#Sammy #Cookies #BakeryStore'),
                          Row(
                            children: <Widget>[
                              Icon(Icons.remove_red_eye),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5.0, right: 5.0),
                                child: Text(
                                  post.data.views.toString(),
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                              Icon(Icons.chat),
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text(
                                  post.data.answerCount.toString(),
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: post.data.answers.length,
                    itemBuilder: (context, index) {
                      return ListTile(title: Text(post.data.answers[index]));
                    }),
              ),
            ],
          );
        },
      ),
    );
  }
}
