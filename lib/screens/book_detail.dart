import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils.dart';

class BookDetailPage extends StatelessWidget {
  final bgColor = hexToColor('#263238');

  @override
  Widget build(BuildContext context) {
    final topContent = Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 10.0),
          height: MediaQuery.of(context).size.height * 0.4,
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/images/hp_dh.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
            margin: EdgeInsets.only(top: 50.0),
            child: Row(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.cancel, color: Colors.white),
                ),
                Spacer(),
                InkWell(
                  onTap: () {},
                  child: Icon(Icons.more_vert, color: Colors.white),
                ),
              ],
            ),
          ),
        )
      ],
    );

    final bottomContent = Container(
      color: bgColor,
      height: MediaQuery.of(context).size.height * 0.6,
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          Text(
            'Harry Potter and the Deathly Hallows - Part I',
            textAlign: TextAlign.center,
            style: GoogleFonts.rubik(
                fontSize: 22.0, fontWeight: FontWeight.bold, height: 1.3),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 12.0),
            child: Text(
              'by J. K. Rowling',
              textAlign: TextAlign.center,
              style: GoogleFonts.rubik(
                  fontSize: 16.0, fontWeight: FontWeight.normal, height: 1.3),
            ),
          ),
          RatingBar(
            initialRating: 3,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: true,
            itemCount: 5,
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            itemSize: 16.0,
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.amber,
            ),
            onRatingUpdate: (rating) {
              print(rating);
            },
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text('Non-Fictional'),
                Text('Adventure'),
                Text('Horror'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ButtonTheme(
              minWidth: MediaQuery.of(context).size.width,
              child: RaisedButton(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'BUY | ' + '\$' + '2.99',
                    style:
                        GoogleFonts.rubik(fontSize: 18.0, color: Colors.black),
                  ),
                ),
                onPressed: () {},
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: Text('Free Trial'),
          ),
          Divider(thickness: 2.0),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Text(
              'The story begins in a time before computers were thought to do more than just Maths. ' +
                  'A boy who’s main passion was to solve logic using what were thoughts of as simple nodes: transistors.' +
                  'The story begins in a time before computers were thought to do more than just Maths.' +
                  'The story begins in a time before computers were thought to do more than just Maths.' +
                  'The story begins in a time before computers were thought to do more than just Maths.' +
                  'The story begins in a time before computers were thought to do more than just Maths.' +
                  'The story begins in a time before computers were thought to do more than just Maths.' +
                  'The story begins in a time before computers were thought to do more than just Maths.' +
                  'The story begins in a time before computers were thought to do more than just Maths.' +
                  'A boy who’s main passion was to solve logic using what were thoughts of as simple nodes: transistors',
              style: GoogleFonts.rubik(fontSize: 14.0),
            ),
          )
        ],
      ),
    );

    return Scaffold(
      body: ListView(
        children: <Widget>[topContent, bottomContent],
      ),
    );
  }
}
