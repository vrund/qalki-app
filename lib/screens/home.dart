import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qalki/widgets/platform_widget.dart';
import '../utils.dart';
import 'dashboard.dart';
import 'drawer_wrapper.dart';
import 'qa.dart';
import 'qalki_book.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  final List<Widget> _tabs = [
    DashboardPage(),
    QalkiBookPage(),
    QuestionAndAnswerPage(),
    DrawerWrapper()
  ];

  Widget _buildIos(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.dashboard),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.question_answer),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text(''),
          )
        ],
      ),
      tabBuilder: (context, index) {
        switch (index) {
          case 0:
            return CupertinoTabView(
              builder: (context) => DashboardPage(),
            );
          case 1:
            return CupertinoTabView(
              builder: (context) => QalkiBookPage(),
            );
          case 2:
            return CupertinoTabView(
              builder: (context) => QuestionAndAnswerPage(),
            );
          case 3:
            return CupertinoTabView(
              builder: (context) => DrawerWrapper(),
            );
          default:
            assert(false, 'Unexpected tab');
            return null;
        }
      },
    );
  }

  Widget _buildAndroid(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            backgroundColor: primaryColor,
            icon: Icon(Icons.dashboard),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.question_answer),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text(''),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
      body: _tabs[_selectedIndex],
    );
  }

  @override
  Widget build(BuildContext context) {
    return PlatformWidget(androidBuilder: _buildAndroid, iosBuilder: _buildIos);
  }
}
