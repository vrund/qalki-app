import 'package:flutter/material.dart';

import 'contact_us.dart';
import 'library.dart';
import 'notifications.dart';
import 'profile.dart';
import 'settings.dart';
import 'your_creations.dart';

class DrawerPageHolder extends StatelessWidget {
  final String selectedDrawerItem;
  DrawerPageHolder({this.selectedDrawerItem});

  @override
  Widget build(BuildContext context) {
    return widgetFromDrawer(selectedDrawerItem);
  }

  widgetFromDrawer(selectedDrawerItem) {
    switch (selectedDrawerItem) {
      case 'profile':
        return ProfileScreen();
        break;
      case 'library':
        return LibraryPage();
        break;
      case 'your-creations':
        return YourCreationsPage();
        break;
      case 'settings':
        return SettingsPage();
        break;
      case 'notifications':
        return NotificationSettingsPage();
        break;
      case 'contact-us':
        return ContactUsPage();
        break;
      default:
        return ProfileScreen();
        break;
    }
  }
}
