import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:qalki/widgets/marketplace_carousel.dart';
import 'package:qalki/widgets/platform_widget.dart';

import '../utils.dart';

class DashboardPage extends StatelessWidget {
  Widget _buildAndroid(BuildContext context) {
    return Container();
  }

  Widget _buildIos(BuildContext context) {
    return CustomScrollView(
      
      slivers: <Widget>[
        // SliverSafeArea(
        //   sliver: SliverPadding(
        //     padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        //     sliver: SliverToBoxAdapter(
        //       child: Container(
        //         height: 80.0,
        //         child: SearchBar(
        //             placeHolder: Text('Search'),
        //             searchBarStyle: SearchBarStyle(
        //                 borderRadius: BorderRadius.all(Radius.circular(10)),
        //                 padding: EdgeInsets.all(0.0)),
        //             onSearch: (String search) {
        //               return null;
        //             },
        //             onItemFound: null),
        //       ),
        //     ),
        //   ),
        // ),
        SliverSafeArea(
          sliver: SliverPadding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            sliver: SliverToBoxAdapter(
              child: Container(
                height: 70.0,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 10,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: CircleAvatar(
                        radius: 40.0,
                        backgroundColor: primaryColorLight,
                        child: CircleAvatar(
                          radius: 28.0,
                          backgroundImage:
                              AssetImage('assets/images/person_1.jpg'),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ),
        SliverPadding(
          padding: const EdgeInsets.all(8.0),
          sliver: SliverToBoxAdapter(
            child: Container(
              height: 30.0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 10,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          side: BorderSide(color: primaryColorLight)),
                      onPressed: () {},
                      child: Text(
                        'POEMS',
                        style: TextStyle(
                            color: primaryColorLight,
                            letterSpacing: 1.5,
                            fontSize: 12.0),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
        SliverStaggeredGrid.count(
          crossAxisCount: 4,
          children: List.generate(20, (int i) {
            return MarketPlaceCarousel(index: i);
          }),
          staggeredTiles: List.generate(20, (int i) {
            return StaggeredTile.fit(2);
          }),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return PlatformWidget(androidBuilder: _buildAndroid, iosBuilder: _buildIos);
  }
}
