import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:qalki/widgets/drawer.dart';

import '../routes.dart';
import '../utils.dart';

class YourCreationsPage extends StatefulWidget {
  @override
  _YourCreationsPageState createState() => _YourCreationsPageState();
}

class _YourCreationsPageState extends State<YourCreationsPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: primaryColor,
          ),
          backgroundColor: Colors.white,
          onPressed: () {},
        ),
        drawer: ProfileDrawer(),
        backgroundColor: hexToColor('#18232A'),
        appBar: AppBar(
          backgroundColor: hexToColor('#18232A'),
          centerTitle: false,
          bottom: TabBar(
            tabs: [
              Tab(
                text: 'PUBLISHED',
              ),
              Tab(
                text: 'UNPUBLISHED',
              ),
            ],
          ),
          title: Text('Your Creations'),
        ),
        body: TabBarView(
          children: [
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 100.0),
                  child: CarouselSlider(
                    height: 400.0,
                    items: [1, 2, 3, 4, 5].map((i) {
                      return Builder(
                        builder: (BuildContext context) {
                          return GestureDetector(
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0),
                                image: new DecorationImage(
                                  image:
                                      new AssetImage("assets/images/hp_dh.jpg"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            onTap: () {
                              Routes.sailor('/your-creations-detailed');
                            },
                          );
                        },
                      );
                    }).toList(),
                  ),
                ),
                Container(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        'Harry Potter and the Deathly Hallows',
                      ),
                    ),
                  ),
                )
              ],
            ),
            Container(
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}
