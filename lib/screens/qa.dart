import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qalki/widgets/platform_widget.dart';

import '../utils.dart';

class Post {
  final String title;
  final String description;

  Post(this.title, this.description);
}

class QuestionAndAnswerPage extends StatelessWidget {
  final dbRef = Firestore.instance;

  Widget _buildIos(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        CupertinoSliverNavigationBar(
          largeTitle: Text('Discuss'),
          leading: Icon(
            Icons.view_headline,
            color: primaryColorLight,
          ),
          trailing: Icon(
            Icons.search,
            color: primaryColorLight,
          ),
        ),
        CupertinoSegmentedControl(
          children: {
            0: Text("Home"),
            1: Text("Bar"),
          },
          onValueChanged: (value) {},
        )
      ],
    );
  }

  Widget _buildAndroid(BuildContext context) {}

  @override
  Widget build(BuildContext context) {
    return PlatformWidget(androidBuilder: _buildAndroid, iosBuilder: _buildIos);
    // return Scaffold(
    //   body: SafeArea(
    //     child: Column(
    //       children: <Widget>[
    //         Container(
    //           height: 100,
    //           child: Padding(
    //             padding: const EdgeInsets.all(8.0),
    //             child: SearchBar<Post>(
    //               onSearch: search,
    //               onItemFound: (Post post, int index) {
    //                 return ListTile(
    //                   title: Text(post.title),
    //                   subtitle: Text(post.description),
    //                 );
    //               },
    //             ),
    //           ),
    //         ),
    //         StreamBuilder(
    //           stream: dbRef
    //               .collection('topics_and_discussion_questions')
    //               .snapshots(),
    //           builder: (context, snapshot) {
    //             if (!snapshot.hasData) return Text('Thinking...');
    //             return Expanded(
    //               child: new StaggeredGridView.countBuilder(
    //                 crossAxisCount: 4,
    //                 itemCount: 20,
    //                 itemBuilder: (BuildContext context, int index) => Padding(
    //                   padding: const EdgeInsets.all(5.0),
    //                   child: new Container(
    //                     decoration: BoxDecoration(
    //                       color: hexToColor('#0D47A1'),
    //                       borderRadius: BorderRadius.all(Radius.circular(15)),
    //                       boxShadow: [
    //                         BoxShadow(
    //                           color: Colors.black26,
    //                           offset: Offset(0.0, 2.0),
    //                           blurRadius: 6.0,
    //                         ),
    //                       ],
    //                     ),
    //                     child: Column(
    //                       children: <Widget>[
    //                         InkWell(
    //                           child: Padding(
    //                             padding: const EdgeInsets.all(10.0),
    //                             child: new Text(
    //                                 snapshot.data.documents[0]['question']),
    //                           ),
    //                           onTap: () {
    //                             Routes.sailor('/detailed-question');
    //                           },
    //                         ),
    //                         Padding(
    //                           padding: const EdgeInsets.all(8.0),
    //                           child: Row(
    //                             mainAxisAlignment:
    //                                 MainAxisAlignment.spaceAround,
    //                             children: <Widget>[
    //                               Icon(Icons.favorite),
    //                               Icon(
    //                                 Icons.edit,
    //                                 size: 35.0,
    //                               ),
    //                               Icon(Icons.save_alt),
    //                             ],
    //                           ),
    //                         )
    //                       ],
    //                     ),
    //                   ),
    //                 ),
    //                 staggeredTileBuilder: (int index) =>
    //                     new StaggeredTile.fit(2),
    //                 mainAxisSpacing: 4.0,
    //                 crossAxisSpacing: 4.0,
    //               ),
    //             );
    //           },
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }

  Future<List<Post>> search(String search) async {
    await Future.delayed(Duration(seconds: 2));
    return List.generate(search.length, (int index) {
      return Post(
        "Title : $search $index",
        "Description :$search $index",
      );
    });
  }
}
