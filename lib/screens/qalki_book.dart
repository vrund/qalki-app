import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils.dart';

class QalkiBookPage extends StatefulWidget {
  @override
  _QalkiBookPageState createState() => _QalkiBookPageState();
}

class _QalkiBookPageState extends State<QalkiBookPage> {
  final dbRef = Firestore.instance;
  final String creamBg = '#EEECE1';
  final String blackColor = '#000000';
  final String kesarPistaBackgroundColor = '#D6FFE6';
  String currentBgColor = '#EEECE1';
  bool _isHorizontalScroll = false;
  bool _showColorPalette = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: hexToColor(this.currentBgColor),
      body: GestureDetector(

        onPanUpdate: (details) {
          if (details.delta.dx < 0) {
            print('next page');
          } else {
            print('previous page');
          }
          
        },
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10),
                child: _readingSettingsBar(),
              ),
              Expanded(
                child: StreamBuilder(
                  stream: dbRef.collection('books').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Text('Thinking...');
                    return Padding(
                      padding: const EdgeInsets.only(left: 25.0, right: 25.0),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: padding5,
                            child: Text(
                              "Chapter ${snapshot.data.documents[0]['chapterNo']}",
                              style: GoogleFonts.cormorant(
                                  color: Colors.black,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: padding5,
                            child: InkWell(
                              child: Text(
                                snapshot.data.documents[0]['chapterTitle'],
                                style: GoogleFonts.cormorant(
                                    color: Colors.black,
                                    fontSize: 30,
                                    fontWeight: FontWeight.w400),
                              ),
                              onTap: () {
                                print('tapped on the title');
                              },
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height / 1.5,
                            child: AutoSizeText(
                              _parseBookData(
                                  snapshot.data.documents[0]['content']),
                              maxLines: 30,
                              style: GoogleFonts.cormorant(
                                color: Colors.black,
                                height: 2,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _readingSettingsBar() {
    return Stack(
      children: <Widget>[
        Visibility(
          visible: _showColorPalette,
          child: Positioned(
            left: 200,
            top: 50,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0.0, 2.0),
                    blurRadius: 6.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    new RawMaterialButton(
                      onPressed: () {
                        setState(() {
                          this.currentBgColor = this.blackColor;
                        });
                      },
                      shape: new CircleBorder(),
                      elevation: 2.0,
                      fillColor: Colors.black,
                    ),
                    new RawMaterialButton(
                      onPressed: () {
                        setState(() {
                          this.currentBgColor = this.creamBg;
                        });
                      },
                      shape: new CircleBorder(),
                      elevation: 2.0,
                      fillColor: hexToColor(this.creamBg),
                    ),
                    new RawMaterialButton(
                      onPressed: () {
                        setState(() {
                          this.currentBgColor = this.kesarPistaBackgroundColor;
                        });
                      },
                      shape: new CircleBorder(),
                      elevation: 2.0,
                      fillColor: hexToColor(this.kesarPistaBackgroundColor),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 50,
          decoration: BoxDecoration(
            color: hexToColor('#0D47A1'),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25),
                bottomRight: Radius.circular(25)),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                offset: Offset(0.0, 2.0),
                blurRadius: 6.0,
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  _buildIconData(
                      _isHorizontalScroll
                          ? Icons.stay_primary_portrait
                          : Icons.stay_primary_landscape,
                      _changeReadingOrientation),
                  _buildIconData(Icons.search, _showSearch),
                  _buildIconData(Icons.color_lens, _toggleColorPalette),
                  _buildIconData(Icons.share, _share),
                  _buildIconData(Icons.bookmark, _bookmarkPage)
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  IconButton _buildIconData(IconData icon, Function onTap) {
    return IconButton(
      icon: Icon(
        icon,
        color: Colors.white,
        size: 24.0,
      ),
      onPressed: onTap,
    );
  }

  void _changeReadingOrientation() {
    setState(() {
      _isHorizontalScroll = !_isHorizontalScroll;
    });
  }

  void _showSearch() {
    print('tapped search');
  }

  void _toggleColorPalette() {
    setState(() {
      this._showColorPalette = !this._showColorPalette;
    });
  }

  void _share() {
    print('tapped share');
  }

  void _bookmarkPage() {
    print('tapped bookmark');
  }

  String _parseBookData(String data) {
    return (data.replaceAll("\\n", "\n\t\t\t\t\t"));
  }
}
