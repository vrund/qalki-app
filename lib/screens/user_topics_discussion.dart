import 'package:flutter/material.dart';
import 'package:qalki/widgets/question_answer_card.dart';

import '../utils.dart';

class UserTopicsDiscussionPage extends StatefulWidget {
  UserTopicsDiscussionPage({Key key}) : super(key: key);

  @override
  _UserTopicsDiscussionPageState createState() =>
      _UserTopicsDiscussionPageState();
}

class _UserTopicsDiscussionPageState extends State<UserTopicsDiscussionPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          backgroundColor: secondaryColor,
          onPressed: () {},
        ),
        appBar: AppBar(
          backgroundColor: hexToColor('#18232A'),
          centerTitle: false,
          bottom: TabBar(
            tabs: [
              Tab(
                text: 'IMPACTS',
              ),
              Tab(
                text: 'QURIOSITY',
              ),
              Tab(
                text: 'SAVED',
              )
            ],
          ),
        ),
        body: TabBarView(
          children: [
            Container(
              color: primaryColor,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      QuestionAnswerOveriewWidget(),
                      QuestionAnswerOveriewWidget(),
                      QuestionAnswerOveriewWidget(),
                      QuestionAnswerOveriewWidget(),
                      QuestionAnswerOveriewWidget(),
                    ],
                  ),
                ),
              ),
            ),
            Container(),
            Container(),
          ],
        ),
      ),
    );
  }
}
