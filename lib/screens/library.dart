import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:qalki/utils.dart';
import 'package:qalki/widgets/drawer.dart';
import 'package:qalki/widgets/membership_promo.dart';
import 'package:qalki/widgets/platform_widget.dart';

class LibraryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> {
  Widget _buildIos(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        CupertinoSliverNavigationBar(
          largeTitle: Text('Library'),
          leading: Icon(
            Icons.view_headline,
            color: primaryColorLight,
          ),
          trailing: Icon(
            Icons.search,
            color: primaryColorLight,
          ),
        ),
        SliverPadding(
          padding: const EdgeInsets.all(8.0),
          sliver: SliverToBoxAdapter(
            child: Container(
              height: 30.0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          side: BorderSide(color: primaryColorLight)),
                      onPressed: () {},
                      child: Text(
                        'POEMS',
                        style: TextStyle(
                            color: primaryColorLight,
                            letterSpacing: 1.5,
                            fontSize: 12.0),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
        SliverStaggeredGrid.count(
          crossAxisCount: 4,
          children: List.generate(20, (int i) {
            return _buildCard();
          }),
          staggeredTiles: List.generate(20, (int i) {
            return StaggeredTile.fit(2);
          }),
        ),
      ],
    );
  }

  Widget _buildAndroid(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        drawer: ProfileDrawer(),
        backgroundColor: hexToColor('#18232A'),
        appBar: AppBar(
          backgroundColor: hexToColor('#18232A'),
          centerTitle: false,
          bottom: TabBar(
            tabs: [
              Tab(
                text: 'TRIALS',
              ),
              Tab(
                text: 'LIBRARY',
              ),
              Tab(
                text: 'ARCHIVED',
              ),
            ],
          ),
          title: Text('Library'),
        ),
        body: TabBarView(
          children: [
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  MembershipPromoWidget(),
                ],
              ),
            ),
            Icon(Icons.directions_transit),
            Icon(Icons.directions_bike),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return PlatformWidget(androidBuilder: _buildAndroid, iosBuilder: _buildIos);
  }

  Widget _buildCard() {
    return Card(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              radius: 20.0,
              backgroundImage: AssetImage('assets/images/person_1.jpg'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
            child: Text(
              'POEM',
              style: TextStyle(
                fontSize: 10.0,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 4.0, top: 8.0),
            child: Text(
              'The Lord of the Rings..',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14.0,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: LinearProgressIndicator(
              value: 10.0,
            ),
          )
        ],
      ),
    );
  }
}
