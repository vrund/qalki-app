import 'package:flutter/material.dart';
import 'package:qalki/widgets/drawer.dart';

import '../utils.dart';

class NotificationSettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: ProfileDrawer(),
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: hexToColor('#18232A'),
        title: Text('Notification Settings'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            SwitchListTile(
              title: const Text(
                'Allow Notifications',
                style: TextStyle(color: Colors.white),
              ),
              value: true,
              onChanged: (bool value) {},
            ),
            SwitchListTile(
              title: const Text(
                'Hide content on lock screen',
                style: TextStyle(color: Colors.white),
              ),
              value: false,
              onChanged: (bool value) {},
            ),
            SwitchListTile(
              title: const Text(
                'New Followers',
                style: TextStyle(color: Colors.white),
              ),
              value: false,
              onChanged: (bool value) {},
            ),
          ],
        ),
      ),
    );
  }
}
