import 'dart:ui';

import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: new Column(
          children: <Widget>[
            _topProfile(),
            // _buildFollowStats(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                  'Interested in a free month of membership? Invite your friends! The more who join, the more you get. '),
            ),
          ],
        ),
      ),
    );
  }

  Widget _topProfile() {
    var scaffoldKey = GlobalKey<ScaffoldState>();
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.4,
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage('assets/images/vrund_profile.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: new BackdropFilter(
            filter: new ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
            child: new Container(
              decoration:
                  new BoxDecoration(color: Colors.white.withOpacity(0.0)),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
            margin: EdgeInsets.only(top: 50.0),
            child: Row(
              children: <Widget>[
                InkWell(
                  onTap: () => scaffoldKey.currentState.openDrawer(),
                  child: Icon(Icons.menu, color: Colors.white),
                ),
                Spacer(),
                InkWell(
                  onTap: () {},
                  child: Icon(Icons.share, color: Colors.white),
                ),
              ],
            ),
          ),
        ),
        Center(child: _buildProfileInfo()),
        _buildFollowStats()
      ],
    );
  }

  Widget _buildFollowStats() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.28,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _buildFollowStatsItem('Followers', '10'),
              _buildFollowStatsItem('Following', '5'),
              _buildFollowStatsItem('Impact', '4')
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildFollowStatsItem(String label, String count) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          label,
          style: TextStyle(fontSize: 16, color: Colors.white),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            count,
            style: TextStyle(
              fontSize: 24,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildProfileInfo() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.1,
        ),
        CircleAvatar(
          backgroundImage: new AssetImage('assets/images/vrund_profile.png'),
          radius: 40.0,
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            'Vrund Patel',
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
        )
      ],
    );
  }
}
