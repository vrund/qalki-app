import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qalki/utils.dart';

import '../routes.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: primaryColor),
            width: double.infinity,
            height: 500.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: Text(
                        'Welcome!',
                        style: GoogleFonts.quicksand(
                            fontSize: 50, color: Colors.white),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    SizedBox(
                      width: 180,
                      child: RaisedButton(
                        color: hexToColor('#534BAE'),
                        child: Text(
                          'SIGN IN',
                          style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontSize: 18.0,
                              letterSpacing: 1.25,
                              fontWeight: FontWeight.bold),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0),
                        ),
                        padding: EdgeInsets.all(10.0),
                        onPressed: () {
                          Routes.sailor('/login');
                        },
                      ),
                    ),
                    SizedBox(
                      width: 180,
                      child: RaisedButton(
                        color: secondaryColor,
                        child: Text(
                          'REGISTER',
                          style: GoogleFonts.roboto(
                              fontSize: 18.0,
                              letterSpacing: 1.25,
                              fontWeight: FontWeight.bold),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0),
                        ),
                        padding: EdgeInsets.all(10.0),
                        onPressed: () {
                          Routes.sailor('/register');
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: Text(
              'By Signing Up, you agree to our Terms & Privacy Policy.',
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Text(
              '© Qalki, Inc. All Rights Reserved.',
            ),
          ),
        ],
      ),
    );
  }
}
