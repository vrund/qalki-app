import 'package:flutter/material.dart';
import 'package:qalki/screens/drawer_wrapper.dart';
import 'package:qalki/services/auth.dart';

import '../utils.dart';

class ProfileDrawer extends StatelessWidget {
  final SelectedDrawerItemModel selectedDrawerItemModel;
  final AuthService _auth = AuthService();

  ProfileDrawer({this.selectedDrawerItemModel});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: new Column(
        children: <Widget>[
          Container(
            color: secondaryColor,
            child: DrawerHeader(
              child: Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      setDrawerItem('/profile');
                    },
                    child: CircleAvatar(
                      radius: 40.0,
                      backgroundImage:
                          AssetImage('assets/images/vrund_profile.png'),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                _buildListItem('Library', Icons.book, () {
                  setDrawerItem('library');
                }),
                _buildListItem('Your Creations', Icons.table_chart, () {
                  setDrawerItem('your-creations');
                }),
                _buildListItem('Settings', Icons.settings, () {
                  setDrawerItem('settings');
                }),
                _buildListItem('Notifications', Icons.notifications, () {
                  setDrawerItem('notifications');
                }),
                _buildListItem('Contact Us', Icons.info, () {
                  setDrawerItem('contact-us');
                }),
                _buildListItem('Log Out', Icons.exit_to_app, _signOut)
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListItem(String label, IconData icon, Function tapHandler) {
    return ListTile(
      leading: new Icon(
        icon,
      ),
      title: Text(
        label,
      ),
      onTap: tapHandler,
    );
  }

  void _signOut() {
    this._auth.signOut();
  }

  void setDrawerItem(String selectedItem) {
    this.selectedDrawerItemModel.updateItem(selectedItem);
    // Navigator.pop(context);
  }
}
