import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils.dart';

class MembershipPromoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        _buildOneMonthMembershipCard(),
        _buildSixMonthMembershipCard(),
        _buildOneYearMembershipCard(),
      ],
    );
  }

  Widget _buildOneMonthMembershipCard() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5.0, 8.0, 5.0, 8.0),
      child: Container(
        height: 150.0,
        width: 350,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Text(
                      '1 Month Plan',
                      style: GoogleFonts.roboto(
                        fontSize: 18.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Text(
                      '\$' + '1.99',
                      style: GoogleFonts.roboto(
                        fontSize: 18.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Text(
                    '1.99 per month value - 30-Day Free Trial',
                    style: GoogleFonts.roboto(
                        color: hexToColor('#8D8D8D'),
                        fontStyle: FontStyle.italic),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Image(
                image: new AssetImage('assets/images/membership_card_1m.png'),
                fit: BoxFit.fitWidth,
              ),
            ),
            Positioned(
              bottom: 10,
              left: 10,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                color: hexToColor('#FFDB95'),
                child: Text(
                  'TRY NOW',
                  style: GoogleFonts.roboto(
                    fontSize: 13.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSixMonthMembershipCard() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 150.0,
        width: 350,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Text(
                      '6 Month Plan',
                      style: GoogleFonts.roboto(
                        fontSize: 18.0,
                        color: hexToColor('#FFA385'),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Text(
                      '\$' + '10.99',
                      style: GoogleFonts.roboto(
                        fontSize: 18.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Text(
                    '1.83 per month value - 30-Day Free Trial',
                    style: GoogleFonts.roboto(
                        color: hexToColor('#8D8D8D'),
                        fontStyle: FontStyle.italic),
                  ),
                ],
              ),
            ),
            Positioned(
              right: -10,
              top: -30,
              child: Image(
                image: new AssetImage('assets/images/6m_membership_ribbon.png'),
                width: 100,
                height: 100,
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Image(
                image: new AssetImage('assets/images/membership_card_6m.png'),
                fit: BoxFit.fitWidth,
              ),
            ),
            Positioned(
              bottom: 10,
              left: 10,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                color: hexToColor('#FFD4C6'),
                child: Text(
                  'TRY NOW',
                  style: GoogleFonts.roboto(
                    fontSize: 13.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildOneYearMembershipCard() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 150.0,
        width: 350,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Text(
                      '1 Year Plan',
                      style: GoogleFonts.roboto(
                        fontSize: 18.0,
                        color: hexToColor('#8D8D8D'),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Text(
                      '\$' + '10.99',
                      style: GoogleFonts.roboto(
                        fontSize: 18.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Text(
                    '1.75 per month value - 30-Day Free Trial',
                    style: GoogleFonts.roboto(
                        color: hexToColor('#8D8D8D'),
                        fontStyle: FontStyle.italic),
                  ),
                ],
              ),
            ),
            Positioned(
              right: -10,
              top: -30,
              child: Image(
                image: new AssetImage('assets/images/1y_membership_ribbon.png'),
                width: 100,
                height: 100,
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Image(
                image: new AssetImage('assets/images/membership_card_1y.png'),
                fit: BoxFit.fitWidth,
              ),
            ),
            Positioned(
              bottom: 10,
              left: 10,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                color: hexToColor('#D6D6FF'),
                child: Text(
                  'TRY NOW',
                  style: GoogleFonts.roboto(
                    fontSize: 13.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
