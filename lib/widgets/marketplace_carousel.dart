import 'package:flutter/material.dart';
import 'package:qalki/utils.dart';

import '../routes.dart';

class MarketPlaceCarousel extends StatelessWidget {
  final int index;

  MarketPlaceCarousel({this.index});

  @override
  Widget build(BuildContext context) {
    return _buildContentCard(context, index);
  }

  Widget _buildContentCard(BuildContext context, int index) {
    return Card(
      margin: EdgeInsets.all(5.0),
      child: Column(
        children: <Widget>[
          Visibility(
            visible: index % 2 != 0,
            child: SizedBox(
              child: Container(
                height: 5.0,
                color: primaryColor,
              ),
            ),
          ),
          Visibility(
            visible: index % 2 == 0,
            child: SizedBox(
              height: 180,
              width: 180,
              child: Container(
                child: Image(
                  image: AssetImage("assets/images/hp_dh.jpg"),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              radius: 28.0,
              backgroundImage: AssetImage('assets/images/person_1.jpg'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
            child: Text(
              'POEM',
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 4.0, top: 8.0),
            child: Text(
              'The Lord of the Rings..',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
                'Why choose oil paint? The best reason to choose any painting medium is that you have an affinityfor it: you have seen it at work, admire it, and want to...'),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.star,
                      color: primaryColor,
                      size: 16.0,
                    ),
                    Text(
                      '3.2',
                      style: TextStyle(fontSize: 12.0),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.sentiment_satisfied,
                      color: primaryColor,
                      size: 16.0,
                    ),
                    Text(
                      '1024',
                      style: TextStyle(fontSize: 12.0),
                    )
                  ],
                ),
                IconButton(
                  icon: Icon(
                    Icons.bookmark,
                    color: secondaryColor,
                  ),
                  onPressed: () {},
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildStoryCard() {}
  Widget _buildPoemCard() {}
  Widget _buildJournalCard() {}
  Widget _buildBookCard() {}

  void navigateToBookDetailedPage() {
    Routes.sailor(
      '/detailed-book',
    );
  }
}
