import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import '../utils.dart';

class BookStats extends StatefulWidget {
  @override
  _BookStatsState createState() => _BookStatsState();
}

class _BookStatsState extends State<BookStats> {
  List<charts.Series> seriesList;
  bool animate;
  String currentBook = 'Lord of the Rings';

  static List<charts.Series<LinearSales, int>> _createSampleData() {
    final myFakeDesktopData = [
      new LinearSales(0, 5),
      new LinearSales(1, 25),
      new LinearSales(2, 100),
      new LinearSales(3, 75),
    ];

    var myFakeTabletData = [
      new LinearSales(0, 10),
      new LinearSales(1, 50),
      new LinearSales(2, 200),
      new LinearSales(3, 150),
    ];

    var fakeThirData = [
      new LinearSales(0, 20),
      new LinearSales(1, 60),
      new LinearSales(2, 40),
      new LinearSales(3, 150),
    ];

    var fakeFourthData = [
      new LinearSales(0, 25),
      new LinearSales(1, 55),
      new LinearSales(2, 45),
      new LinearSales(3, 75),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Desktop',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: myFakeDesktopData,
      )
        // Configure our custom bar target renderer for this series.
        ..setAttribute(charts.rendererIdKey, 'customArea'),
      new charts.Series<LinearSales, int>(
        id: 'Tablet',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: myFakeTabletData,
      ),
      new charts.Series<LinearSales, int>(
        id: 'Desktop',
        colorFn: (_, __) => charts.MaterialPalette.purple.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: fakeThirData,
      ),
      new charts.Series<LinearSales, int>(
        id: 'Desktop',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: fakeFourthData,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Column(
          children: <Widget>[
            DropdownButton(
                value: this.currentBook,
                items: <String>['Lord of the Rings', 'Two', 'Free', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (String value) {
                  setState(() {
                    this.currentBook = 'Lord of the Rings';
                  });
                }),
            Container(
              height: 300.0,
              child: charts.LineChart(_createSampleData(),
                  animate: animate,
                  customSeriesRenderers: [
                    new charts.LineRendererConfig(
                        // ID used to link series to this renderer.
                        customRendererId: 'customArea',
                        includeArea: true,
                        stacked: true),
                  ]),
            ),
            Card(
              color: hexToColor('#3061FF'),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  leading: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.favorite,
                      size: 25.0,
                    ),
                  ),
                  title: Text(
                    'BUYERS',
                    style: TextStyle(fontSize: 12.0),
                  ),
                  subtitle: Text(
                    '2,059',
                    style:
                        TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Card(
              color: hexToColor('#3061FF'),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  leading: Icon(
                    Icons.person_add,
                    size: 45.0,
                  ),
                  title: Text(
                    'ACTIVE READERS',
                    style: TextStyle(fontSize: 12.0),
                  ),
                  subtitle: Text(
                    '60',
                    style:
                        TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Card(
              color: hexToColor('#3061FF'),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  leading: Icon(
                    Icons.remove_red_eye,
                    size: 45.0,
                  ),
                  title: Text(
                    'TRIAL USERS',
                    style: TextStyle(fontSize: 12.0),
                  ),
                  subtitle: Text(
                    '991',
                    style:
                        TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Card(
              color: hexToColor('#3061FF'),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  leading: Icon(
                    Icons.library_books,
                    size: 45.0,
                  ),
                  title: Text(
                    'PAGES READ',
                    style: TextStyle(fontSize: 12.0),
                  ),
                  subtitle: Text(
                    '6,000',
                    style:
                        TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
