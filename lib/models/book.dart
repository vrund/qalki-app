import 'package:flutter/material.dart';
import 'user.dart';

class Book {
  Book({
    @required String id,
    @required String title,
    @required String description,
    @required User author,
    @required String imageUrl,
    @required double price,
    @required int rating,
    @required List<String> tags,
  });
}

class BookWithContent extends Book {
  BookWithContent({
    @required BookContent content,
    @required String privacy,
    @required bool isPublished,
  });
}

class BookContent {
  BookContent({@required List<Chapter> chapters});
}

class Chapter {
  Chapter({
    @required String chapterTitle,
    @required int chapterOrder,
    @required Story story,
  });
}

class Story {
  Story({
    @required List<Page> pages,
  });
}

class Page {
  Page({
    @required String storyText,
    @required List<String> quotes,
    @required List<BookQuestionAnswer> questionAns,
  });
}

class BookQuestionAnswer {
  BookQuestionAnswer({
    @required String questionText,
    @required List<String> answers,
  });
}
