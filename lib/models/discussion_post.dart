import 'package:cloud_firestore/cloud_firestore.dart';

class DiscussionPost {
  final String question;
  final List<String> answers;
  final String author;
  final int likes;
  final int views;
  final int answerCount;

  DiscussionPost(this.question, this.answers, this.author, this.likes,
      this.views, this.answerCount);

  DiscussionPost.fromJson(DocumentSnapshot json)
      : question = json.data['question'],
        answers = List.from(json.data['answers']),
        author = json.data['author'],
        likes = json.data['likes'],
        views = json.data['views'],
        answerCount = json.data['answerCount'];

  Map<String, dynamic> toJson() => {
        'question': question,
        'answers': answers,
        'author': author,
        'likes': likes,
        'views': views,
        'answerCount': answerCount,
      };
}

class DicussionPostQuestion {
  final String question;

  DicussionPostQuestion(this.question);
}

class DicussionPostAnswer {
  final String author;
  final String answer;

  DicussionPostAnswer(this.author, this.answer);
}
