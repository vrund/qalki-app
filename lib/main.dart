import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qalki/screens/auth_wrapper.dart';
import 'package:qalki/services/auth.dart';
import 'package:qalki/utils.dart';

import 'locator.dart';
import 'models/user.dart';
import 'routes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Routes.createRoutes();
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: primaryColor,
          accentColor: secondaryColor
        ),
        debugShowCheckedModeBanner: false,
        home: AuthWrapper(),
        onGenerateRoute: Routes.sailor.generator(),
        navigatorKey: Routes.sailor.navigatorKey,
      ),
    );
  }
}
