import 'package:qalki/screens/book_detail.dart';
import 'package:qalki/screens/book_pricing.dart';
import 'package:qalki/screens/detailed_question.dart';
import 'package:qalki/screens/detailed_your_creations.dart';
import 'package:qalki/screens/register.dart';
import 'package:qalki/screens/sign_in.dart';
import 'package:qalki/screens/user_topics_discussion.dart';
import 'package:sailor/sailor.dart';

class Routes {
  static final sailor = Sailor();

  static void createRoutes() {
    sailor.addRoutes([
      SailorRoute(
        name: '/register',
        builder: (context, args, params) {
          return Register();
        },
      ),
      SailorRoute(
        name: '/login',
        builder: (context, args, params) {
          return SignIn();
        },
      ),
      SailorRoute(
        name: '/detailed-book',
        defaultTransitions: [
          SailorTransition.slide_from_bottom,
        ],
        defaultTransitionDuration: Duration(milliseconds: 500),
        builder: (context, args, params) {
          return BookDetailPage();
        },
      ),
      SailorRoute(
        name: '/your-creations-detailed',
        builder: (context, args, params) {
          return YourCreationsDetailedPage();
        },
      ),
      SailorRoute(
        name: '/pricing',
        builder: (context, args, params) {
          return BookPricingPage();
        },
      ),
      SailorRoute(
        name: '/detailed-question',
        builder: (context, args, params) {
          return DetailedQuestionPage();
        },
      ),
      SailorRoute(
        name: '/user-topics-discussion',
        builder: (context, args, params) {
          return UserTopicsDiscussionPage();
        },
      ),
    ]);
  }
}
