import 'package:flutter/material.dart';

final primaryColor = hexToColor('#1A237E');
final primaryColorLight = hexToColor('#534BAE');
final secondaryColor = hexToColor('#FFB300');
final padding10 = EdgeInsets.all(10.0);
final padding5 = EdgeInsets.all(5.0);

Color hexToColor(String code) {
  return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}