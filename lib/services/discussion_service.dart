import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:qalki/services/api.dart';

class DiscussionService implements Api {
  final Firestore _db = Firestore.instance;
  CollectionReference ref;
  String get path => '';

  DiscussionService(String path) {
    ref = _db.collection(path);
  }


  @override
  Future<DocumentReference> addDocument(Map data) {
    // TODO: implement addDocument
    return null;
  }

  @override
  Future<QuerySnapshot> getDataCollection() {
    // TODO: implement getDataCollection
    return null;
  }

  @override
  Future<DocumentSnapshot> getDocumentById(String id) {
    return ref.document(id).get();
  }


  @override
  Future<void> removeDocument(String id) {
    // TODO: implement removeDocument
    return null;
  }

  @override
  Stream<QuerySnapshot> streamDataCollection() {
    // TODO: implement streamDataCollection
    return null;
  }

  @override
  Future<void> updateDocument(Map data, String id) {
    // TODO: implement updateDocument
    return null;
  }

}
