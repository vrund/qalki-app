import 'package:get_it/get_it.dart';
import 'package:qalki/services/discussion_service.dart';

GetIt locator = GetIt();

void setupLocator() {
  locator.registerSingleton(DiscussionService('detailed_discussion_posts'));
}
